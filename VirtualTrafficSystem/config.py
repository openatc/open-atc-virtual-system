
"""
一些配置参数
"""

# SUMO相关配置
sumoCfg = ''  # SUMO配置文件
sumoNet = ''  # SUMO路网文件
sumoRou = ''  #
sumoAdd = ''  # SUMO检测器文件
sumoPath = ''  # SUMO文件路径
steplength = 0.1  # 仿真步长 s
multiple = 1  # 仿真加速倍数
cinductionloopid = ''  # 路网中心检测器id, 用于订阅检测器数据
radius = 0  # 订阅范围半径
# 路口关联配置表
deviceCfg = ''  # 仿真路口关联表所在文件夹目录
# 虚拟信号机配置文件
vartualControlCfg = ''  # 虚拟信号机所在文件夹目录
# 接收信号灯数据的UDP端口
tlsport = 0
# 仿真时长s
simulationDuration = 999999
# outputName
outputName = ''

######################################################
# 仿真路网相关信息
# 仅在.net.xml及.det.xml文件 改变时重新生成
######################################################
junctions = list()
# 路口及其包含车道id
junction_lanes = dict()
# 路口及其包含检测器id
junction_detectors = dict()

######################################################
# 仿真路口关联设备信息、通道配置
# sumo初始化时从 sumo\devices.csv 读入生成
######################################################
devices = dict()
devices2junction = dict()
addrs = dict()
junction_channels = dict()
channelsList = dict()
junction_linknum = dict()
junction_groupnum = dict()

