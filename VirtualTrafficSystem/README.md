### 简介

    OpenATC虚拟环境是一个基于SUMO的交通仿真系统，主要功能包括路网编辑、车流仿真、信控方案配置、流量统计等。可用于信号机的上路装机前的软件功能测试、各种智能配时方案的效果验证等。

### 准备工作

    安装 Python 3.8+  
    安装 SUMO 1.12+  
    安装部署[OpenATC-Admin](https://gitee.com/openatc/open-atc-admin)
    获取[虚拟信号机](https://gitee.com/openatc/open-atc-source/releases)程序

### 文档目录

    open-atc-virtual-system
    ├─VirtualTrafficSystem                                  // 联动程序及场景文件目录
    |  ├─scenario                                           // 场景文件夹
    │  │  │ param.ini                                       // 联动配置文件
    │  │  ├─6.3VirtualTrafficSystemTest                     // 虚拟场景，含SUMO文件,虚拟信号机文件,.csv配置文件,.ini配置文件和.py场景启动文件
    │  │  │    ├─sumo                                       // SUMO相关文件
    │  │  │    │ 5junc.net.xml                              // SUMO路网文件
    │  │  │    │ 5junc.add.xml                              // SUMO检测器附加文件
    │  │  │    │ 5junc.rou.xml                              // SUMO车流文件
    │  │  │    │ DealayStatistic.py                         // 仿真输出文件处理脚本
    │  │  │    │ run.sumocfg                                // SUMO-GUI启动文件
    │  │  │    ├─VirtualSignalControl                       // 场景所需所有虚拟信号机文件目录
    │  │  │    │         ├─start.py                         // 虚拟信号机批量启动脚本
    │  │  │    │         ├─kill.py                          // 虚拟信号机批量关闭脚本
    │  │  │    │         ├─originfiles                      // 虚拟信号机主程序文件夹
    │  │  │    │         │ OpenATCMainCtlManager.exe        // 虚拟信号机可执行程序
    │  │  │    │         │ .dll                             // 虚拟信号机相关dll依赖
    │  │  │    │         ├─13001                            // 虚拟信号机配置文件夹
    │  │  │    │         │ OpenATCTZParam.json              // 虚拟信号机信控方案文件
    │  │  │    │         │ OpenATCHWParam.json              // 虚拟信号机配置文件
    │  │  │    │         │ ConfigPort.xml                   // 虚拟信号机配置文件
    │  │  │    │         │ LocalConfig.xml                  // 虚拟信号机配置文件
    │  │  │    │         └─13002                            // 虚拟信号机配置文件夹
    │  │  │    ├─.ini                                       // 联动配置文件
    │  │  │    ├─.py                                        // 场景启动脚本
    │  │  │    └─devices.csv                                // 信号机设备与仿真路口对应表
    |  | config.py                                          // SUMO相关配置，一些常量及待初始化的参数定义
    |  | dettlssumo.py                                      // 信号机通讯的SUMO类定义，从仿真订阅感应线圈数据
    |  | junction.py                                        // 仿真交叉口类定义，包含路口及对应设备的一些操作
    |  | main_dettls.py                                     // 信号机通讯的主入口，包含检测器数据封装和信号灯状态数据处理
    |  | queuedata.py                                       //  一些消息队列的定义
    |  | udp.py                                             // UDP_Client和UDP_Server定义

### 使用说明
    进入对应的场景中，正确配置参数后，运行对应场景中的.py，会启动一个该场景下的sumo-gui程序。
    仿真脚本会将仿真场景的检测器数据发送给信号机，同时会接收信号机发送的信号灯状态，实时改变仿真场景的信号灯状态。 
 