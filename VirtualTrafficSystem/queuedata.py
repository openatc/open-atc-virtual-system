"""
消息队列
"""
from queue import Queue
det_queue = Queue()  # 检测器数据队列，用于仿真检测器数据发送给虚拟信号机
tls_queue = Queue()  # 原始灯色数据队列，用于将从虚拟信号机接收的灯色消息发送给灯色解析程序
ryg_queue = Queue()  # 解析后灯色数据队列，用于灯色解析程序向仿真发送给灯色消息
