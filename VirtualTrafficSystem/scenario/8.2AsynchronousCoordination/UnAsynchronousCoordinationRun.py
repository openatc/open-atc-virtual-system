# -*-coding:utf-8-*-
# coding=utf-8
# Copyright (c) 2023 kedacom
# OpenATC is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# Auther : LTF
# Date : 2023/3/27 11:12
# File : run.py

from sys import path
import os
fileCatalogue = os.path.abspath('../..')
path.insert(0,r'%s'%(fileCatalogue))
from main_dettls import Main
import threading
outputName = os.path.basename(__file__)

if __name__ == "__main__":
    main = Main('UnAsynchronousCoordinationParam.ini',outputName)
    threads = []
    process_detqueue_thread = threading.Thread(target=main.process_detqueue)
    tls_server_thread = threading.Thread(target=main.tlsudpser.run)
    process_tlsqueue_thread = threading.Thread(target=main.process_tlsqueue_grouped)
    threads.append(process_detqueue_thread)
    threads.append(tls_server_thread)
    threads.append(process_tlsqueue_thread)
    for thread in threads:
        thread.start()
    main.start_sumo()
