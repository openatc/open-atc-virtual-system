# -*-coding:utf-8-*-
# coding=utf-8
# Copyright (c) 2023 kedacom
# OpenATC is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# Auther : LTF
# Date : 2023/3/29 14:10
# File : start.py
import os

os.chdir('originfiles')
exeFile = os.getcwd()+'\OpenATCMainCtlManager.exe'
os.chdir('..')

currentContentsFiles = os.listdir()
for x in range(len(currentContentsFiles)):
    try:
        os.chdir(currentContentsFiles[x])
        if 'config' in os.listdir():
            os.startfile(exeFile)
        os.chdir('..')
    except:
        pass
