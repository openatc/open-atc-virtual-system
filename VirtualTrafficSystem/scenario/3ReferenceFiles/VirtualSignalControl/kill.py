# -*-coding:utf-8-*-
# coding=utf-8
# Copyright (c) 2023 kedacom
# OpenATC is licensed under Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
# http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# Auther : LTF
# Date : 2023/3/29 14:59
# File : kill.py

import os
import psutil
def get_pid(name):
    pids = psutil.process_iter()
    for pid in pids:
        if pid.name() == name:
            os.popen('taskkill.exe /pid:' + str(pid.pid))
get_pid("OpenATCMainCtlManager.exe")