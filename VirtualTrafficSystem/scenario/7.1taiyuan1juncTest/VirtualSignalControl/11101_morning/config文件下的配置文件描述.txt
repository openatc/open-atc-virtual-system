一：虚拟信号机配置文件
config文件下的配置文件需换成你原先自己的相对应文件
config文档下包含
1. configPort.xml
/********************************************/
<Config>  
	<CfgPort>16016</CfgPort> 			//虚拟信号机路口号
	<DetectorPort>20030</DetectorPort> 		//仿真端口
	<VideoDetectorPort>20030</VideoDetectorPort> 	//仿真端口
	<SimulateIP>192.168.15.175</SimulateIP> 	//仿真IP
	<SimulatePort>6667</SimulatePort> 		//仿真端口号
</Config>  
/********************************************/

2. LocalConfig.xml
/********************************************/
<LogConfig>
    <LogLevel>CRITICAL</LogLevel>
    <FileMaxKb>1024</FileMaxKb>
    <FileMaxNum>2</FileMaxNum>
    <PrintToScreen>1</PrintToScreen>
</LogConfig>
<LanguageConfig>
    <Language>0</Language>
</LanguageConfig>
<SpeedyRunConfig>
    <SpeedyRun>0</SpeedyRun>	//虚拟信号机是否加速运行 0：不加速 1：二倍速 2：五倍速 3：十倍速
</SpeedyRunConfig>
<SimStartTime>			//虚拟信号机启动时间
    <Year>2022</Year>
    <Month>6</Month>
    <Day>6</Day>
    <Hour>7</Hour>
    <Minute>05</Minute>
</SimStartTime>
<CommThread>			//信号机与配置工具使用通讯协议选择 
    <Comm>1</Comm>
</CommThread>
/********************************************/

3. OpenATCHWParam.json
4. OpenATCTZParam.json

