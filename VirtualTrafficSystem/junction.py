"""
路口类定义，仿真路口相关属性及操作
"""

import json
import datetime
import config

from udp import UDP_Client


class Junction:
    def __init__(self, junctionid):
        self.junctionid = junctionid            # 仿真路口ID
        self.deviceid = self.getDeviceID()      # 信号机ID
        self.tlLogicid = junctionid             # 仿真信号灯ID
        self.addr = self.getAddr()              # 信号机通信地址(IP:Port)
        self.lanes = self.getLanes()            # 仿真路口包含车道(进入路口)
        self.detectors = self.getDetectors()    # 仿真路口检测器
        self.groupnum = self.getGroupnum()  # 路口仿真的通道数量
        #self.flowRecord_file = open(self.junctionid + '.txt', 'a', encoding='utf-8')     ##flow推送文件

    def getDeviceID(self):
        deviceid = config.devices[self.junctionid]
        return deviceid

    def getAddr(self):
        addr = config.addrs[self.junctionid]
        return addr

    def getLanes(self):
        lanes = config.junction_lanes[self.junctionid]
        return lanes

    def getDetectors(self):
        detecors = config.junction_detectors[self.junctionid]
        return detecors



    def getCreateTime(self, frameno):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        now = now[:-3]
        simTime = frameno * config.steplength * 1000  # 毫秒数
        s, ms = divmod(simTime, 1000)
        m, s = divmod(s, 60)
        h, m = divmod(m, 60)
        h %= 24
        strSimTime = ("%02d:%02d:%02d.%03d" % (h, m, s, ms))
        now = now[:10] + ' ' + strSimTime
        return now

    def getLocalTime(self):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        now = now[:-3]
        now = now[:10] + ' ' + now[11:]
        return now
    def getGroupnum(self):
        return config.junction_groupnum[self.junctionid]
